from django.db import models
from paintstore.fields import ColorPickerField
from django.utils.text import slugify

class title(models.Model):
    title = models.CharField(max_length=200, blank = True, null = True)
    description = models.TextField("beschreibung", max_length=500, blank = True, null = True)

    class Meta:
        abstract = True

class headline(models.Model):
    headline = models.CharField("schlagzeile", max_length=200, blank = True, null = True)

    class Meta:
        abstract = True

class slug(title, models.Model):
    slug = models.CharField(max_length=50, blank = True, null = True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        if len(self.slug) >= 50:
            self.slug = self.slug[:50]
        super(slug, self).save()


class image(models.Model):
    image = models.ImageField(upload_to='images/')
    alt = models.CharField("Alternativ Text", max_length=100)

    class Meta:
        abstract = True

class content(models.Model):
    content = models.TextField(max_length=2000, blank=True, null=True)

    class Meta:
        abstract = True


COLORS = (
        ('#2c77ae', 'Blau'),
        ('#d98428', 'Orange'),
        ('#9FC752', 'Gruen'),
        ('#706F6f', 'Dunkel Grau'),
        ('#BEBFC3', 'Hell Grau'),
        ('#000000', 'Schwarz'),
        ('#FFFFFF', 'Weiss')
    )

class color(models.Model):
    color = models.CharField(max_length="8", blank = True, null = True, default = "#2c77ae", verbose_name=u'Farbe')

    class Meta:
        abstract=True


class email(models.Model):
    email = models.EmailField(max_length=100)

    class Meta:
        abstract=True