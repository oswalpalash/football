# #README# #

This is an API in Django for FIFA World Cup Data in JSON. It uses the json data from http://worldcup.sfg.io/

##REQUIREMENTS##

django,
django-mobile,
django-compressor,
south

### HOW TO SETUP ###

Change the flag for settings in manage.py with the name of your settings file.

If you want custom settings, declare your file in de_football/file_name.py and call it in manage.py


Runserver : python manage.py runserver