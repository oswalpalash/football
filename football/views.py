from django.shortcuts import get_object_or_404, get_list_or_404, render
from django.template import RequestContext
import requests
#from django.template.defaultfilters import slugify

from .models import *
def api_view(request, api=""):
    from json import loads
    template = "football/football.html"
    
    #enter the url from the api here.
    base_url = "http://worldcup.sfg.io"#/%s" % api
    
    # combine the base path with the string from the address url
    text = "%s/%s" %(base_url, api)
    
    # this assumes the request returns json < -----
    # Since that was wrong, we have to get the text(like you pointed out) which WILL be json
    # then load it into a string
    json = loads(requests.get(text).text)
    from pprint import pprint

    pprint(dir(json))
    
    context = {
        'content': json,
    }

    return render(request, template, context, context_instance = RequestContext(request))
# def api_view(request, api=""):
	# template = "football/football.html"
	
	# #enter the url from the api here.
	# base_url = "http://worldcup.sfg.io"
	
    # # this assumes the request returns json
	# json = requests.get('%s/%s' % (base_url, api)).text
	# context = {
		# 'content': json,
	# }

	# return render(request, template, context, context_instance = RequestContext(request))