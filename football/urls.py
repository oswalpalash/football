from django.conf.urls import patterns, url
from .views import api_view

urlpatterns = patterns('',
    url(r'^(?P<api>[\w-]+)/?', api_view, name = "api")
    )