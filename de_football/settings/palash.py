from .settings import *

ROOT_PATH = 'E:/Python/football'

MEDIA_ROOT = '%s/media/' % ROOT_PATH

MEDIA_URL = '/media/'

STATIC_ROOT = '%s/static/' % ROOT_PATH

STATIC_URL = '/static/'

STATICFILES_DIRS =('%s/local' % ROOT_PATH)


TEMPLATE_DIRS = (
    '%s/templates' % ROOT_PATH
)
